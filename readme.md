## TRABALHO DE PÓS-GRADUAÇÃO WEB

### ATIVIDADE COMPLEMENTAR

### ALUNOS
<p>Lameck Fernandes</p>
<p>Viviana Fernandes</p>

### INSTALAÇÃO

```
   1  sudo apt-get install apache2
   2  sudo apache2ctl configtest
   3  sudo systemctl restart apache2
   4  sudo apt-get install mysql-server
   5  mysql_secure_installation
   6  sudo mysql_secure_installation
   7  sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
   
   8  sudo apt-get install php libapache2-mod-php php-mysql
   9  sudo systemctl restart apache2
   10  sudo systemctl status apache2
   11  apt-cache search php- | less
   12  apt-cache show php-cli
   13  sudo apt-get install php-cli
   14  sudo nano /var/www/html/info.php
   15  sudo apt-get install phpmyadmin php-mbstring php-gettext
   16  sudo phpenmod mbstring
   17  sudo phpenmod mcrypt
   18  sudo systemctl restart apache2
   19  sudo mysql
   20  sudo apt install php libapache2-mod-php php-mysql
   21  sudo systemctl restart apache2
   22  sudo apt-get install nodejs
   23  sudo apt-get install npm
   25  node -v
   26  npm -v
   27  npm install --save socket.io
   28  npm install --save socket.io-client
  
```
### USO
```
Inicie o Laravel: php artisan serve --host=IP --port=8000
Na home, troque o IP pelo ip iniciado

Configure o nome do banco no .env para uso do login no sistema.

Inicie o servidor node: node server.js
Basta entrar na página home da dasboard após logar no sistema
```
